#ifndef FLATPAK_H
#define FLATPAK_H
#include <string>
#include <QObject>
#include <QProcess>
class Flatpak :QObject
{
    Q_OBJECT
public:
    Flatpak(std::string appName , QObject *parent = nullptr);
    int  GetInfo();
    int GetPer();
private :
    int run(QStringList);

private:
    QProcess *command;
    std::string appName ;
};

#endif // FLATPAK_H
