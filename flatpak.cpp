
#include <QString>
#include <QDebug>
#include "flatpak.h"

Flatpak::Flatpak( std::string appName , QObject *parent): QObject(parent)
{
    this->appName =appName;
    command = new QProcess(this);
}

int  Flatpak::GetPer()
{
    QStringList args;
    args << "info" <<"--show-permissions"<< QString(this->appName.c_str()) ;
    return this->run(args);
}
int  Flatpak::GetInfo()
{
    QStringList args;
    args << "info" << QString(this->appName.c_str());
    return this->run(args);

}

int  Flatpak::run(QStringList args)
{
    return command->execute("flatpak", args  );
    //command->waitForFinished();

}


